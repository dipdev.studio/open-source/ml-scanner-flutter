
import 'package:ml_scanner/presentation/ui/base/mvvm/stateful/app_screen.dart';

import 'splash_screen_model.dart';
import 'splash_screen_view.dart';
import 'splash_screen_view_model.dart';

class SplashScreen extends AppScreen<SplashScreenModel, SplashScreenView,
    SplashScreenViewModel> {
  SplashScreen() : super(new SplashScreenModel());

  @override
  SplashScreenView initView(SplashScreenModel model) {
    return SplashScreenView(model);
  }

  @override
  SplashScreenViewModel initViewModel(SplashScreenView view) {
    return SplashScreenViewModel(view);
  }
}
