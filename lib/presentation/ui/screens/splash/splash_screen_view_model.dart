import 'dart:async';

import 'package:ml_scanner/presentation/ui/base/mvvm/stateful/app_view_model.dart';
import 'package:ml_scanner/presentation/ui/screens/home/home_screen.dart';
import 'package:package_info/package_info.dart';

import 'splash_screen_model.dart';
import 'splash_screen_view.dart';

class SplashScreenViewModel
    extends AppViewModel<SplashScreenModel, SplashScreenView> {
  SplashScreenViewModel(SplashScreenView view) : super(view);

  @override
  init() async {
    super.init();
    _setTimer();
    _updateVersionApp();
  }

  void _setTimer() async {
    const twentyMillis = const Duration(seconds: 3);
    new Timer(twentyMillis, () {
      _navigate();
    });
  }

  void _updateVersionApp() async {
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      String version = packageInfo.version;
      String buildNumber = packageInfo.buildNumber;
      model.versionApp = version + " (" + buildNumber + ")";
      view.updateUI();
    });
  }

  void _navigate() async {
    await view.navigateTo(model.context, new HomeScreen(), true);
  }
}
