import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ml_scanner/presentation/ui/base/mvvm/stateful/app_view_model.dart';

import 'home_screen_model.dart';
import 'home_screen_view.dart';

class HomeScreenViewModel
    extends AppViewModel<HomeScreenModel, HomeScreenView> {
  HomeScreenViewModel(HomeScreenView view) : super(view);

  @override
  init() async {
    super.init();
    model.selectImage.setCallback(getSelectedImage);
  }

  Future getSelectedImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    loadingShow();
    model.visionImage = FirebaseVisionImage.fromFile(image);
    model.scanResult = "";
    await decodeBarcode(model.visionImage);
    //await decodeCloudLabels(model.visionImage);
    await decodeFaces(model.visionImage);
    await decodeLabels(model.visionImage);
    await decodeVisionText(model.visionImage);
    loadingHide();
  }

  Future decodeBarcode(FirebaseVisionImage visionImage) async {
    final List<Barcode> barcodes =
    await model.barcodeDetector.detectInImage(visionImage);
    if (barcodes != null && barcodes.length > 0) {
      model.scanResult += "Barcodes: ${barcodes[0].rawValue} \n";
    } else {
      model.scanResult += "Barcodes: Not found \n";
    }
    view.updateUI();
  }

  Future decodeCloudLabels(FirebaseVisionImage visionImage) async {
    final List<ImageLabel> cloudLabels =
    await model.cloudLabeler.processImage(visionImage);
    if (cloudLabels != null && cloudLabels.length > 0) {
      model.scanResult += "CloudLabels: ${cloudLabels[0].text} \n";
    } else {
      model.scanResult += "CloudLabels: Not found \n";
    }
    view.updateUI();
  }

  Future decodeFaces(FirebaseVisionImage visionImage) async {
    final List<Face> faces = await model.faceDetector.processImage(visionImage);
    if (faces != null && faces.length > 0) {
      model.scanResult += "Found ${faces.length} people \n";
    } else {
      model.scanResult += "Faces: Not found \n";
    }
    view.updateUI();
  }

  Future decodeLabels(FirebaseVisionImage visionImage) async {
    final List<ImageLabel> labels =
    await model.labeler.processImage(visionImage);
    if (labels != null && labels.length > 0) {
      model.scanResult += "Labels: ${labels[0].text} \n";
    } else {
      model.scanResult += "Labels: Not found \n";
    }
    loadingHide();
    view.updateUI();
  }

  Future decodeVisionText(FirebaseVisionImage visionImage) async {
    final VisionText visionText =
    await model.textRecognizer.processImage(visionImage);
    if (visionText != null) {
      model.scanResult += "VisionText: ${visionText.text}";
    } else {
      model.scanResult += "VisionText: Not found";
    }
    loadingHide();
    view.updateUI();
  }

  @override
  void viewDisposed() {
    model.barcodeDetector.close();
    model.cloudLabeler.close();
    model.faceDetector.close();
    model.labeler.close();
    model.textRecognizer.close();
  }
}
