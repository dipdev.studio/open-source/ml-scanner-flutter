import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter_arhitecture_helper/presentation/ui/mvvm/utils/base_model_utils.dart';
import 'package:ml_scanner/presentation/ui/base/mvvm/stateful/app_model.dart';

class HomeScreenModel extends AppModel {
  OnCallCommand selectImage = OnCallCommand();

  FirebaseVisionImage visionImage;
  String scanResult = "";

  /*final BarcodeDetector barcodeDetector = FirebaseVision.instance
      .barcodeDetector(
          BarcodeDetectorOptions(barcodeFormats: BarcodeFormat.qrCode));*/
  final BarcodeDetector barcodeDetector = FirebaseVision.instance
      .barcodeDetector();
  final ImageLabeler cloudLabeler = FirebaseVision.instance.cloudImageLabeler();
  final FaceDetector faceDetector = FirebaseVision.instance.faceDetector();
  final ImageLabeler labeler = FirebaseVision.instance.imageLabeler();
  final TextRecognizer textRecognizer =
      FirebaseVision.instance.textRecognizer();
}
