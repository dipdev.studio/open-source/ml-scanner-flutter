import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ml_scanner/presentation/res/colors.dart';
import 'package:ml_scanner/presentation/res/dimens.dart';
import 'package:ml_scanner/presentation/ui/base/mvvm/stateful/app_view.dart';

import 'home_screen_model.dart';

class HomeScreenView extends AppView<HomeScreenModel> {
  HomeScreenView(HomeScreenModel model) : super(model);

  @override
  Widget getView(BuildContext context) {
    return new Scaffold(
        body: new Container(
      height: double.maxFinite,
      width: double.maxFinite,
      decoration: new BoxDecoration(
        color: ColorsApp.background,
      ),
      child: new Center(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new MaterialButton(
              onPressed: () {
                model.selectImage.onCall();
              },
              color: ColorsApp.accent,
              child: Padding(
                padding: EdgeInsets.only(
                    top: DimensApp.paddingSmallExtra,
                    bottom: DimensApp.paddingSmallExtra),
                child: Text('Scan',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold)),
              ),
            ),
            new Padding(padding: EdgeInsets.all(DimensApp.paddingSmall)),
            new Text('Results:'),
            new Padding(padding: EdgeInsets.all(DimensApp.paddingSmall)),
            new Text(model.scanResult),
          ],

        ),
      ),
    ));
  }
}
