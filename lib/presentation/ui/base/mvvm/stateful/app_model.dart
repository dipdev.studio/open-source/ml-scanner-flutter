import 'package:flutter/widgets.dart';
import 'package:flutter_arhitecture_helper/presentation/ui/mvvm/stateful/base_model.dart';

import 'app_view.dart';
import 'app_view_model.dart';

class AppModel extends BaseModel {
  AppViewModel appViewModel;
  AppView appView;

  double get screenHeight {
    return MediaQuery.of(view.context).size.height;
  }

  double get screenWidth {
    return MediaQuery.of(view.context).size.width;
  }
}
