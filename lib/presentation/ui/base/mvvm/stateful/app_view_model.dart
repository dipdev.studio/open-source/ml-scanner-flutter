import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_arhitecture_helper/presentation/ui/mvvm/stateful/base_view.dart';
import 'package:flutter_arhitecture_helper/presentation/ui/mvvm/stateful/base_view_model.dart';

import 'app_model.dart';

abstract class AppViewModel<M extends AppModel, V extends BaseView<M>>
    extends BaseViewModel<M, V> {

  AppViewModel(V view) : super(view);

  @override
  init() async {
    super.init();
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.dark.copyWith(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.light,
          statusBarBrightness: Brightness.dark),
    );
  }

  @override
  void viewRefresh() {
    super.viewRefresh();
  }
}
