import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'colors.dart';
import 'dimens.dart';

class ThemeApp {

  static TextStyle get picoGreyTextStyle {
    return TextStyle(
      fontFamily: 'GoogleSans',
      fontSize: DimensApp.textSizePico,
      color: Colors.grey[400],
    );
  }

  static ThemeData data = ThemeData(
    primaryColorDark: ColorsApp.primaryDark,
    primaryColor: ColorsApp.primary,
    accentColor: ColorsApp.accent,
    canvasColor: Colors.transparent,
  );
}
