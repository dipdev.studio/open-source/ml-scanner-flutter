import 'package:flutter/rendering.dart';

class ColorsApp {
  static const Color primary = Color(0xFF147AA8);
  static const Color primaryDark = Color(0xFF106285);
  static const Color accent = Color(0xFF147AA8);
  static const Color blue = Color(0xFF2e7dfa);
  static const Color purple = Color(0xFFbe78fa);
  static const Color gray = Color(0xFF777777);
  static const Color pink = Color(0xFFebbde4);
  static const Color background = Color(0xFFf1f2f3);
}
