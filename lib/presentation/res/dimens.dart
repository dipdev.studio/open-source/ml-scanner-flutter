class DimensApp {
  //Paddings
  static const double paddingPico = 2.0;
  static const double paddingMicro = 5.0;
  static const double paddingSmall = 10.0;
  static const double paddingSmallExtra = 15.0;
  static const double paddingMiddle = 20.0;
  static const double paddingMiddleExtra = 25.0;
  static const double paddingNormal = 30.0;
  static const double paddingNormalExtra = 40.0;
  static const double paddingBig = 50.0;
  static const double paddingBigExtra = 65.0;
  static const double paddingLarge = 80.0;
  static const double paddingLargeExtra = 100.0;

  //Margin
  static const double marginPico = 2.0;
  static const double marginMicro = 5.0;
  static const double marginSmall = 10.0;
  static const double marginSmallExtra = 15.0;
  static const double marginMiddle = 20.0;
  static const double marginMiddleExtra = 25.0;
  static const double marginNormal = 30.0;

  //Text sizes
  static const double textSizePico = 10.0;
  static const double textSizeLittle = 11.0;
  static const double textSizeSmall = 14.0;
  static const double textSizeMiddle = 16.0;
  static const double textSizeMiddleExtra = 18.0;
  static const double textSizeNormal = 20.0;
  static const double textSizeBig = 22.0;
  static const double textSizeLarge = 24.0;

 }
