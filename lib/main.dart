import 'dart:async';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_arhitecture_helper/presentation/ui/base_app/base_app.dart';
import 'package:flutter_arhitecture_helper/presentation/ui/base_app/base_app_config.dart';
import 'package:ml_scanner/presentation/res/theme.dart';
import 'package:ml_scanner/presentation/ui/screens/splash/splash_screen.dart';

final FirebaseAnalytics analytics = FirebaseAnalytics();

void main() async {
  Crashlytics.instance.enableInDevMode = false;
  FlutterError.onError = (FlutterErrorDetails details) {
    FlutterError.dumpErrorToConsole(details);
    Zone.current.handleUncaughtError(details.exception, details.stack);
    Crashlytics.instance.onError(details);
  };

  var theme = ThemeApp.data;
  var appConfig = new BaseAppConfig(
      isMaterial: true,
      title: 'ML Scanner',
      theme: theme,
      color: theme.primaryColor,
      debugShowCheckedModeBanner: false,
      supportedLocales: [Locale("en")],
      onGenerateTitle: (BuildContext context) => 'ML Scanner',
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
      home: SplashScreen());
  var app = new BaseApp(appConfig);
  runApp(app);
}
